<?php

namespace Proj1;

class Translator
{
	private $lang;

	public function __construct($lang = 'en')
	{
		$this->lang = $lang;
	}

	public function translate($msg)
	{
		include __DIR__.'/shared/translations/'.$this->lang.'.php';
		if (isset($messages[$msg])) {
			return $messages[$msg];
		}
		return 'UNTRANSLATED("'.$msg.'")';
	}
}
