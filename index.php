<?php

include __DIR__.'/translator.php';
include __DIR__.'/shared/mail.php';
include __DIR__.'/shared/utils.php';

use Proj1\Translator;

class Proj1
{
	private $translator;

	public function __construct()
	{
		$this->translator = new Translator();
	}

	public function run()
	{
		echo 'project 1<br>';
		echo $this->translator->translate('test');
		echo '<hr>';

		$cpEmail = new \Shared\CPEmail($this->translator);
		$cpEmail->sendMail('tomas.babicky@slayoutmaschine.de', 'test');
		echo '<hr>';

		\Shared\Utils::first();
		//echo $utils::first();
	}
}

$c = new Proj1;
$c->run();
